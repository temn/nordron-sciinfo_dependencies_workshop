import coordinator
coordinator.perform_fs(
        array_name, features,
        labeled_feature,
        labels,
        preprocessing_options,
        feature_selection_method,
        classifier,
        feature_selection_options=None,
        classification_options=None):
